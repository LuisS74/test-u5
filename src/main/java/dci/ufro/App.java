package dci.ufro;

import dci.ufro.model.Network;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(App.class.getName());
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        String message = results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%";
        logger.log(Level.INFO, message );
    }
}
